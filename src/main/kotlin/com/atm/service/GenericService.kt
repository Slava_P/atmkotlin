package com.atm.service

abstract class GenericService <M, D> {

    abstract fun create(newObject:M):M

    abstract fun createFromDTO(newObjectDTO:D):M

    abstract fun update(newObject: M): M

    abstract fun updateFromDTO(id:Long, newObjectDTO: D):M

    abstract fun delete(id: Long)

    abstract fun getOne(id: Long):M

    abstract fun getAll(): List<M>
}