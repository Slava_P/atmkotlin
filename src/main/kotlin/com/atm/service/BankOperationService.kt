package com.atm.service

import com.atm.dto.BankOperationDTO
import com.atm.model.BankOperation
import com.atm.repository.AccountRepository
import com.atm.repository.BankOperationRepository
import org.springframework.stereotype.Service

@Service
class BankOperationService(
    val bankOperationRepository: BankOperationRepository,
    val accountRepository: AccountRepository
): GenericService<BankOperation, BankOperationDTO>() {

    override fun create(newObject: BankOperation): BankOperation {
        return bankOperationRepository.save(newObject)
    }

    override fun createFromDTO(newObjectDTO: BankOperationDTO): BankOperation {
        val newBankOperation = BankOperation()
        val account = accountRepository.findAccountByAccountNumber(newObjectDTO.accountNumber)
        newBankOperation.id = newObjectDTO.id
        newBankOperation.account = account
        newBankOperation.operationType = newObjectDTO.operationType
        newBankOperation.sum = newObjectDTO.sum
        newBankOperation.receiverCardNumber = newObjectDTO.receiverCardNumber
        newBankOperation.dateOperation = newObjectDTO.dateOperation
        newBankOperation.description = newObjectDTO.description

        return bankOperationRepository.save(newBankOperation)
    }

    override fun update(newObject: BankOperation): BankOperation {
        return bankOperationRepository.save(newObject)
    }

    override fun updateFromDTO(id: Long, newObjectDTO: BankOperationDTO): BankOperation {
        val updateBankOperation = getOne(id)
        val account = accountRepository.findAccountByAccountNumber(newObjectDTO.accountNumber)
        updateBankOperation.id = newObjectDTO.id
        updateBankOperation.account = account
        updateBankOperation.operationType = newObjectDTO.operationType
        updateBankOperation.sum = newObjectDTO.sum
        updateBankOperation.receiverCardNumber = newObjectDTO.receiverCardNumber
        updateBankOperation.dateOperation = newObjectDTO.dateOperation
        updateBankOperation.description = newObjectDTO.description

        return bankOperationRepository.save(updateBankOperation)
    }

    override fun delete(id: Long) {
        bankOperationRepository.delete(getOne(id))
    }

    override fun getOne(id: Long): BankOperation {
        return bankOperationRepository.findById(id).orElseThrow { ClassNotFoundException("Нет банковской операции с id = $id") }
    }

    override fun getAll(): List<BankOperation> {
        return bankOperationRepository.findAll()
    }

}