package com.atm.model


enum class OperationType (var type: String) {

    DEPOSIT ("Внесение"),
    WITHDRAW ("Снятие"),
    TRANSFER_IN ("Зачисление"),
    TRANSFER_OUT ("Перевод");

}