package com.atm.repository

import com.atm.model.Account
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AccountRepository: JpaRepository<Account ,Long> {

    fun findAccountByAccountNumber(accountNumber: Int): Account

}