package com.atm.repository

import com.atm.model.BankOperation
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BankOperationRepository: JpaRepository<BankOperation, Long> {
}