package com.atm.dto

import com.atm.model.Account
import lombok.AllArgsConstructor
import lombok.Getter
import lombok.NoArgsConstructor
import lombok.Setter

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
data class AccountDTO constructor(val account: Account) {

    var id: Long? = null
    var accountNumber: Int = id.toString().hashCode()
    var firstName: String
    var lastName: String
    var middleName: String?
    var cardNumber: String?
    var pincode: Int?
    var balance: Double


    init {
        this.id = account.id
        this.accountNumber = account.accountNumber
        this.firstName = account.firstName
        this.lastName = account.lastName
        this.middleName = account.middleName
        this.cardNumber = account.cardNumber
        this.pincode = account.pincode
        this.balance = account.balance
    }

}