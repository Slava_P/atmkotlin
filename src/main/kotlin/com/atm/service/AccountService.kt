package com.atm.service

import com.atm.dto.AccountDTO
import com.atm.model.Account
import com.atm.repository.AccountRepository
import org.springframework.stereotype.Service


@Service
class AccountService(
    var accountRepository: AccountRepository)
    : GenericService<Account, AccountDTO>() {

    override fun create(newObject: Account): Account {
        return accountRepository.save(newObject)
    }

    override fun createFromDTO(newObjectDTO: AccountDTO): Account {
        val newAccount = Account()
        newAccount.id = newObjectDTO.id
        newAccount.accountNumber = newObjectDTO.accountNumber
        newAccount.firstName = newObjectDTO.firstName
        newAccount.lastName = newObjectDTO.lastName
        newAccount.middleName = newObjectDTO.middleName
        newAccount.cardNumber = newObjectDTO.cardNumber
        newAccount.pincode = newObjectDTO.pincode
        newAccount.balance = newObjectDTO.balance

        return accountRepository.save(newAccount)
    }

    override fun update(newObject: Account): Account {
        return accountRepository.save(newObject)
    }

    override fun updateFromDTO(id: Long, newObjectDTO: AccountDTO): Account {
        val updateAccount = getOne(id)
        updateAccount.firstName = newObjectDTO.firstName
        updateAccount.lastName = newObjectDTO.lastName
        updateAccount.middleName = newObjectDTO.middleName
        updateAccount.cardNumber = newObjectDTO.cardNumber
        updateAccount.pincode = newObjectDTO.pincode
        updateAccount.balance = newObjectDTO.balance

        return accountRepository.save(updateAccount)

    }

    override fun delete(id: Long) {
        accountRepository.delete(getOne(id))
    }

    override fun getOne(id: Long): Account {
        return accountRepository.findById(id).orElseThrow { ClassNotFoundException("Нет аккаунта с id = $id") }
    }

    override fun getAll(): List<Account> {
        return accountRepository.findAll()
    }

}

