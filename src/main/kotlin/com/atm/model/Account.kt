package com.atm.model

import com.fasterxml.jackson.annotation.JsonInclude
import jakarta.persistence.*

@Entity
@Table(name = "accounts")
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_seq")
    @SequenceGenerator(name = "account_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    var id: Long? = null

    @Column(name = "account_number", nullable = false, unique = true)
    var accountNumber: Int = id.toString().hashCode()

    @Column(name = "first_name", nullable = false)
    var firstName: String = ""

    @Column(name = "last_name", nullable = false)
    var lastName: String = ""

    @Column(name = "middle_name")
    var middleName: String? = null

    @Column(name = "card_number")
    var cardNumber: String? = ""

    @Column(name = "pincode")
    var pincode: Int? = null

    @Column(name = "balance")
    var balance: Double = 0.0

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    @JsonInclude
    var bankOperations: HashSet<BankOperation> = HashSet()


}