package com.atm.model

import com.fasterxml.jackson.annotation.JsonInclude
import jakarta.persistence.*
import lombok.Getter
import lombok.NoArgsConstructor
import lombok.Setter
import java.time.LocalDateTime

@Entity
@Table
@NoArgsConstructor
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
class BankOperation {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bank_operation_seq")
    @SequenceGenerator(name = "bank_operation_seq", allocationSize = 1)
    @Column(name = "id", nullable = false)
    var id: Long? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    var account: Account? = null

    @Column(name = "operation_type", nullable = false)
    lateinit var operationType: OperationType

    @Column(name = "sum")
    var sum: Double = 0.0

    @Column(name = "receiver_card_number")
    var receiverCardNumber: String? = null

    @Column(name = "date_operation")
    lateinit var dateOperation: LocalDateTime

    @Column(name = "description")
    var description: String? = null

}