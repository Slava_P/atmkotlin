package com.atm.dto

import com.atm.model.BankOperation
import com.atm.model.OperationType
import java.time.LocalDateTime

class BankOperationDTO(val bankOperation: BankOperation) {

    var id: Long? = null
    var accountNumber: Int
    var operationType: OperationType
    var sum: Double
    var receiverCardNumber: String?
    var dateOperation: LocalDateTime
    var description: String?

    init {
        this.id = bankOperation.id
        this.accountNumber = bankOperation.account!!.accountNumber
        this.operationType = bankOperation.operationType
        this.sum = bankOperation.sum
        this.receiverCardNumber = bankOperation.receiverCardNumber
        this.dateOperation = bankOperation.dateOperation
        this.description = bankOperation.description

    }

}