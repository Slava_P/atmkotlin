package com.atm

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AtmKotlinApplication

fun main(args: Array<String>) {
    runApplication<AtmKotlinApplication>(*args)
}
